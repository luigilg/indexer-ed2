#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


#define MAX_PALAVRAS 1000000
#define COMPRIMENTO 100


struct Palavras {
    char palavra[COMPRIMENTO];
    int freq;
} heap[MAX_PALAVRAS];

int totalPalavrasGlobal[100];  
int frequenciaPalavraGlobal[100]; 

int indicador = 0;
int heapEsq(int input) { return (input * 2 + 1); }
int heapDir(int input) { return (input * 2 + 2); }
int heapParent(int child) { return (child - 1) / 2; }

int ehNumero(char c);
int ehLetra(char c);
char lowerCase(char c);
int addSeTiver(char *input);
void add(char *input);
void reheapFrom(int child);    
void reheap(int input);
void troca(int a, int b);
int lerPalavra(char *input);
void resultado(int k);
struct Palavras remove_max();
void processaArq(FILE *fin, int k);
void freqPalavra(char *palavra, FILE *fin);
void freqPalavraSearch(char *palavra, FILE *fin, int i);
int contaOcorrencias(const char *termo, FILE *fin);

void processaArq(FILE *fin, int k) {
    char input[COMPRIMENTO] = {0};
    while (lerPalavra(input)) {
        if (strlen(input) > 1) {
            if (!addSeTiver(input)) {
                add(input);
            }
        }
    }
    // Reparei que reheapar uma unica vez ao final era mais eficiente do que garantir o heap em tempo de
    // execução nos casos de teste, talvez em outras situações não seja o mesmo
    for (int i = (indicador - 1) / 2; i >= 0; i--) {
        reheap(i);
    }

    resultado(k);

    float elapsed = (float)(clock()) / CLOCKS_PER_SEC;
    printf("\nTempo: %f seg\n", elapsed);
}


void freqPalavra(char *palavra, FILE *fin) {
    char input[COMPRIMENTO] = {0};
    int freq = 0;
    while (lerPalavra(input)) {
        if (!strlen(input))
            continue;
        if (strcmp(input, palavra) == 0)
            freq++;
    }

    printf("%s - %d vezes\n", palavra, freq);

    float elapsed = (float)(clock()) / CLOCKS_PER_SEC;
    printf("\nTempo: %f segundos\n", elapsed);
}

void freqPalavraSearch(char *palavra, FILE *fin, int i) {
    char input[COMPRIMENTO] = {0};
    int freq = 0;
    int totalPalavras = 0;
    while (lerPalavra(input)) {
        totalPalavras++;
        if (!strlen(input))
            continue;
        if (strcmp(input, palavra) == 0)
            freq++;
    }

    if (freq > 0){
        frequenciaPalavraGlobal[i] = freq;
        totalPalavrasGlobal[i] = totalPalavras;
    }
   
}

void resultado(int k) {
    int t = 0;
    struct Palavras saida;
    while (k--) {
        saida = remove_max();       
        t = saida.freq;
        printf("%s - %d vezes\n", saida.palavra, saida.freq);
    }
    saida = remove_max();
    while (t == saida.freq && indicador) {
        printf("%s - %d vezes\n", saida.palavra, saida.freq);
        saida = remove_max();
    }
}

void troca(int a, int b) {
    int t;
    char t_c[COMPRIMENTO];

    t = heap[a].freq;
    heap[a].freq = heap[b].freq;
    heap[b].freq = t;

    strcpy(t_c, heap[a].palavra);
    strcpy(heap[a].palavra, heap[b].palavra);
    strcpy(heap[b].palavra, t_c);
}

void add(char *input) {    
    heap[indicador].freq = 1;
    strcpy(heap[indicador].palavra, input);
    indicador++;    
}

int addSeTiver(char *input) {
    int i;
    for (i = 0; i < indicador; i++) {
        if (!strcmp(heap[i].palavra, input)) {
            heap[i].freq++;
            reheapFrom(i);
            return 1;
        }
    }
    return 0;
}

void reheapFrom(int child) {
    int input = heapParent(child);
    if (heap[input].freq >= heap[child].freq || input < 0) return;
    troca(input, child);
    reheapFrom(input);
}

void reheap(int input) {
    int child = heapEsq(input);
    if (child >= indicador) return;
    if ((heap[child].freq < heap[child + 1].freq) && (child < (indicador - 1))) child++;
    if (heap[input].freq >= heap[child].freq) return;
    troca(input, child);
    reheap(child);
}

struct Palavras remove_max() {
    indicador--;
    if (indicador != 0) {
        troca(0, indicador);
        reheap(0);
    }
    return heap[indicador];
}

int lerPalavra(char *input) {
    char c;
    int ind = 0;
    while (scanf("%c", &c) != EOF) {
        if (!ehLetra(c) && !ehNumero(c)) {
            input[ind] = '\0';
            return 1;
        }
        input[ind++] = lowerCase(c);
    }
    return 0;
}

int ehNumero(char c){ 
    if( c>='0' && c<='9')    return 1;
    return 0; 
} 

int ehLetra(char c) {
    return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

char lowerCase(char c) {
    if (!ehLetra(c)) return c;
    return (c | (1 << 5));
}

int contaOcorrencias(const char *termo, FILE *fin) {
    int ocorrencias = 0;
    char palavra[100];

    while (fscanf(fin, "%s", palavra) != EOF) {
        if (strstr(palavra, termo) != NULL) {
            ocorrencias++;
        }
    }

    return ocorrencias;
}



int main(int argc, char **argv) {
    if (argc < 4) {
        printf("Uso: %s [--freq N | --freq-word PALAVRA] ARQUIVO\n", argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "--freq") == 0) {
        int k = atoi(argv[2]);
        if (k <= 0) {
            printf("N deve ser maior que 0.\n");
            return 1;
        }

        FILE *fin = freopen(argv[3], "r", stdin);
        processaArq(fin, k);
        fclose(fin);
    } else if (strcmp(argv[1], "--freq-word") == 0) {
        if (argc != 4) {
            printf("Uso: %s --freq-word PALAVRA ARQUIVO\n", argv[0]);
            return 1;
        }

        FILE *fin = freopen(argv[3], "r", stdin);
        if (strlen(argv[2]) <= 2){
            printf("So lerei palavras com mais de 2 caracteres.");
        } else {
            freqPalavra(argv[2], fin);
        }
        fclose(fin);
    } else if (strcmp(argv[1], "--search") == 0) {
            if (argc < 4) {
                printf("Uso: %s --search TERMO ARQUIVO [ARQUIVO ...]\n", argv[0]);
                return 1;
            }

            const char *termo = argv[2];

            int nDocs = argc - 3;
            
            for (int i = 3; i < argc; ++i) {
                FILE *fin = freopen(argv[i], "r", stdin);
                if (fin == NULL) {
                    printf("Erro ao abrir o arquivo %s\n", argv[i]);
                    continue;  
                } else{
                    
                    freqPalavraSearch(argv[2],fin,i);
                }   
                fclose(fin);
            }
            double TF;
            float IDF;
            int docNaoAparece = 0;
             for (int i = 3; i < nDocs+3; ++i) {
                
               if (totalPalavrasGlobal[i] > 0){
                
               }
               else docNaoAparece++;
               
            }
            double TFIDF = 0;
            for (int i = 3; i < nDocs+3; ++i) {
                TFIDF = 0;
                TF = 0;
                IDF = 0;
               if (totalPalavrasGlobal[i] > 0){
              
                TF = (double)((double)frequenciaPalavraGlobal[i] / (double)totalPalavrasGlobal[i]);
                printf("\n\nFrequencia do termo = %i \nTotal de Palavras = %i", frequenciaPalavraGlobal[i], totalPalavrasGlobal[i]);
                
                IDF = log10((double)((double)nDocs/(double)(nDocs-docNaoAparece)));
                TFIDF = TF*IDF;
                printf("\nTF = %f\nIDF = %f",TF,IDF);
                printf("\nTFIDF = %f\n\n" ,TFIDF);
                printf("Este foi do arquivo: %s \n\n----------\n",argv[i]);
               }
            }
            
            printf("\nNumero total de documentos: %i\nNumero de documentos que nao apresentavam o termo: %i\n\n",nDocs,docNaoAparece);

        } else {
        printf("Uso: %s [--freq N | --freq-word PALAVRA] ARQUIVO\n", argv[0]);
        return 1;
    }

    return 0;
    
}
